import { totpService } from "../services/TOTPService";

test('TOTPService::Generate Key.', () => {
 const secret = totpService.generateSecret();
 expect(secret).not.toBe('');
});

test('TOTPService::Generate token', async () =>{
    const token = await totpService.generateToken(totpService.generateSecret());
    expect(token).not.toBe('');
    expect(token.length).toBe(6);
});

test('TOTPService::Generate QRURL', () =>{
    const QRURL = totpService.getQRURL('username', totpService.generateSecret());
    expect(QRURL).not.toBe('');
});

test('TOTPService::ValidateKeysTrue', async () => {
    const secret = totpService.generateSecret();
    const token = await totpService.generateToken(secret);
    const isValid = totpService.checkToken(token, secret);
    expect(isValid).toBe(true);
})

test('TOTPService::ValidateKeysFalse', async () => {
    const secret = totpService.generateSecret();
    const isInvalid = totpService.checkToken('123456', secret);
    expect(isInvalid).toBe(false);
})
