import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Toolbar from '@mui/material/Toolbar';
import Paper from '@mui/material/Paper';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import { totpService } from '../services/TOTPService';
import { Step1 } from './TOTPSConfigurationScreen/Step1';
import { Step2 } from './TOTPSConfigurationScreen/Step2';
import { Step3 } from './TOTPSConfigurationScreen/Step3';
import { Footer } from './Footer';

export default function TOTPSConfigurationScreen() {
    const steps = ['Tus datos', 'Codigo QR', 'Validación'];
    const [activeStep, setActiveStep] = React.useState(0);
    const [username, setUsername] = React.useState<string>('');
    const [secret] = React.useState<string>(totpService.generateSecret())
    const [isValidationSucecssfully, setIsValidationSuccessfully] = React.useState<boolean>(false);

    const handleNext = () => setActiveStep(activeStep + 1);

    const handleBack = () => setActiveStep(activeStep - 1);

    const generareQRCode = () =>{
        const qrURL = totpService.getQRURL(username, secret);
        return qrURL;
    }

    const handleRemainingTime = () =>  totpService.getTimeRemaining();

    const handleValidation = (token: string) => {
        const isValid = totpService.checkToken(token, secret);
        setIsValidationSuccessfully(isValid);
    }
    const handleReset = () =>  {
        // Clean up operatons TODO
        setActiveStep(0);
    }

    function getStepContent(step: number) {
        switch (step) {
            case 0:
                return <Step1 onChangeUsername={(username) => setUsername(username)}/>;
            case 1:
                return <Step2 qrURL={generareQRCode()} />;
            case 2:
                return <Step3 
                 isValidationSucecssfully={isValidationSucecssfully}  // Typo isValidationSucec[e]ssfully!
                 onChangeToken={(token) => handleValidation(token)}
                 getRemainingTime={handleRemainingTime}
                 generateToken={async() => await totpService.generateToken(secret)}
                />;
            default:
                throw new Error('Unknown step');
        }
    }

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar
                position="absolute"
                color="default"
                elevation={0}
                sx={{
                    position: 'relative',
                    borderBottom: (t) => `1px solid ${t.palette.divider}`,
                }}
            >
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        PRÁCTICA TOTPS. ANGEL G. 1 CIBE.
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
                <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
                    <Typography component="h1" variant="h4" align="center">
                        Configuración TOTP
                    </Typography>
                    <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>

                    <React.Fragment>
                        <Box ml={3}>
                            {getStepContent(activeStep)}
                                <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                                    {activeStep !== 0 && (
                                        <Button onClick={handleBack} sx={{ mt: 3, ml: 1 }}>
                                            Volver
                                        </Button>
                                    )}
                                    {activeStep === steps.length - 1 ?
                                        <Button
                                            variant="contained"
                                            onClick={handleReset}
                                            sx={{ mt: 3, ml: 1 }}
                                        >
                                            Reiniciar
                                        </Button> :
                                        <Button
                                            disabled={username.length < 3}
                                            variant="contained"
                                            onClick={handleNext}
                                            sx={{ mt: 3, ml: 1 }}
                                        >
                                            Siguiente
                                        </Button>
                                    }
                                </Box>
                            </Box>
                    </React.Fragment>
                </Paper>
                <Footer />
            </Container>
        </React.Fragment>
    );
}