import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Modal, Typography } from "@mui/material";
import Grid from "@mui/material/Grid"
import TextField from "@mui/material/TextField"
import React from "react";

interface IStep3 {
    onChangeToken: (username: string) => void,
    getRemainingTime: () => number,
    generateToken: () => Promise<string>,
    isValidationSucecssfully: boolean,
}

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export const Step3 = (props: IStep3): JSX.Element => {
    const [token, setToken] = React.useState<string>('');
    const [isVisibleValidationModal, setIsVibileValidationModal] = React.useState<boolean>(false);
    const [timeRemaining, setTimeRemaining] = React.useState<number>(0);
    const [tokenKey, setTokenKey] = React.useState<string>(""); // Test OK token

    React.useEffect(() => {
        const getReaminingTime = () => {
            const remainingtime = props.getRemainingTime()
            setTimeRemaining(remainingtime);
            if (remainingtime === 30) getToken();
        }
        const getToken = async () => {
            const newToken = await props.generateToken();
            setTokenKey(newToken);
        }
        getToken();
        setInterval(getReaminingTime, 1000);
    }, [props])

    React.useEffect(() => {
        props.onChangeToken(token)
    }, [token, props])

    return <Grid container spacing={2}>
        <Grid xs={8} mb={1}>
            Escribe el código que muestra la app.
        </Grid>
        <Grid xs={12} mb={1}>
            Sera válido los siguientes {timeRemaining} segundos.
        </Grid>
        <Grid xs={8} mb={1}>
            <TextField
                type="number"
                fullWidth
                id="standard-basic"
                label="Token"
                variant="standard"
                value={token}
                onChange={e => setToken(e.target.value)}
            />
        </Grid>
        <Grid xs={8} mt={1}>
            Cuando estes listo pulsa:
            <Button
                onClick={() => setIsVibileValidationModal(true)}
                disabled={token.length < 6} // 6 -> Token expected length
            >
                Validar
            </Button>
        </Grid>
        <Grid xs={8} mt={1}>
            <Accordion>
                <AccordionSummary
                    expandIcon={<>Abrir</>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography>Pulsa para mostrar un token</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography >
                        Este es el token que deberías ver en Google Authenticator:
                        <Typography variant="overline"> {tokenKey}</Typography>
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </Grid>

        <Modal
            open={isVisibleValidationModal}
            onClose={() => { setToken(''); setIsVibileValidationModal(false) }}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Validación de token
                    {props.isValidationSucecssfully ? <img width="20px" src="./assets/checkOk.png" alt="codigo valido" />
                        : <img width="30px" src="./assets/checkNoOk.png" alt="codigo valido" />}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                    {props.isValidationSucecssfully ? '¡Tu código es valido!' : '¡Tu código NO es valido!'}
                </Typography>
                <Typography mt={2}>
                    Pulsa fuera de la ventana para cerrar y validar otro token.
                </Typography>
            </Box>
        </Modal>
    </Grid>
}