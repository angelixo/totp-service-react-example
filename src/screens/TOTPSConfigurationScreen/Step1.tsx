import Grid from "@mui/material/Grid"
import TextField from "@mui/material/TextField"
import React from "react";

interface IStep1 {
    onChangeUsername: (username: string) => void    
}
export const Step1 = (props: IStep1): JSX.Element => {
    const  [username, setUsername] = React.useState<string>('');
    
    React.useEffect(() =>  {
        props.onChangeUsername(username)
    }, [username, props])
    return <Grid container spacing={2}>
        <Grid xs={8} mb={1}>
            Para comenzar escribe tu nombre de usuario.
        </Grid>
        <Grid xs={8} mb={1}>
            <TextField 
                fullWidth 
                id="standard-basic"
                label="Nombre de usuario" 
                variant="standard"  
                value={username}
                onChange={e => setUsername(e.target.value)}
            />
        </Grid>
        <Grid xs={8} mt={1}>
            Cuando estes listo pulsa en <i>Siguiente</i>.
        </Grid>
    </Grid>
}