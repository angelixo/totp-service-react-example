import { Accordion, AccordionSummary, Typography, AccordionDetails } from "@mui/material";
import Grid from "@mui/material/Grid"
import QRCode from "react-qr-code";

interface IStep2 {
    qrURL: string
}

export const Step2 = (props: IStep2): JSX.Element => {

    return <Grid container spacing={2}>
        <Grid xs={12} mb={2}>
            Escanea el código con la app Google Authenticator
        </Grid>
        <Grid xs={12} mb={1}>
            <div style={{ height: "auto", margin: "0 auto", maxWidth: 250, width: "100%" }}>
                <QRCode
                    size={256}
                    style={{ height: "auto", maxWidth: "100%", width: "100%" }}
                    value={props.qrURL}
                    viewBox={`0 0 256 256`}
                />
            </div>
        </Grid>
        <Grid xs={8} mt={2}>
            Cuando estes listo pulsa en <i>Siguiente</i>.
        </Grid>
        <Grid xs={12} mt={1}>
            <Accordion>
                <AccordionSummary
                    expandIcon={<>Abrir</>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography>Pulsa para mostrar la URL</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography overflow={'scroll'} >
                        {props.qrURL}
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </Grid>
    </Grid>
}