import { Typography } from "@mui/material";

export function Footer() {
    return (
        <Typography variant="body2" color="text.secondary" align="center">
            {'angel.g -  1 cibe.'}
        </Typography>
    );
}