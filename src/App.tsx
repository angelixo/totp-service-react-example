import { ThemeProvider, createTheme } from '@mui/material/styles';
import TOTPSConfigurationScreen from './screens/TOTPSConfigurationScreen';
 // Polyfill issue
import { Buffer } from 'buffer';
// @ts-ignore
window.Buffer = Buffer;

function App() {

  const darkTheme = createTheme({
    palette: {
      mode: 'dark',
    },
  });

  return (
      <ThemeProvider theme={darkTheme}>
        <TOTPSConfigurationScreen />
      </ThemeProvider>
  );
}

export default App;
