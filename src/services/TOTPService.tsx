import { authenticator } from 'otplib';

interface ITOTPService {
    generateSecret: () => string,
    getTimeRemaining: () => number,
    checkToken: (token: string, secret: string) => boolean,
    generateToken: (secret: string) => Promise<string>,
    getQRURL: (username: string, secret: string) => string,
}

const totpService : ITOTPService = {
    getTimeRemaining: function (): number {
        return authenticator.timeRemaining();
    },
    checkToken: function (token: string, secret: string): boolean {
        return authenticator.check(token, secret);
    },
    generateToken: async function (secret: string): Promise<string> {
        return await authenticator.generate(secret);
    },
    getQRURL: function (username: string, secret: string): string {
        return authenticator.keyuri(username, "PracticaAngelG", secret);
    },
    generateSecret: function (): string {
        return authenticator.generateSecret();
    }
}

export { totpService };
